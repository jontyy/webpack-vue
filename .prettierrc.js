module.exports = {
  endOfLine: 'lf',
  printWidth: 100,
  semi: true,
  singleQuote: true,
  tabWidth: 2,
  trailingComma: 'all',
  Semicolons: true,
  vueIndentScriptAndStyle: true,
};
