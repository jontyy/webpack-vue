# 功能
1. 实现移动端开发模板

# PS
1. 只兼容移动端，pc端见基于react和webpack进行开发的模板

# 基于
1. vue
2. webpack
3. node
4. vue-router
5. vuex
6. eslint
7. vant
8. stylelint
9. qs
10. lodash
11. git
12. less
13. axios


# 配置
1. stylelint 规则  https://cloud.tencent.com/developer/section/1489630
2. less   http://lesscss.org/#
3. vue    https://cn.vuejs.org/
4. webpack   https://www.webpackjs.com/concepts/
5. git     https://morvanzhou.github.io/
6. 配置eslint等  https://www.jb51.net/article/135191.htm       
7. vue中的最佳实践    https://github.com/coppyC/blog/issues/1
8. qs使用文档  https://www.npmjs.com/package/qs
9. lodash使用文档   https://www.lodashjs.com/docs/latest
10. axios文档  https://github.com/axios/axios