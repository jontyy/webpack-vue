# Changelog

项目的更改记录在该文件中。

## [Unreleased]

- :feat:

- :fix:

- :change:

## 2019-12-18

- :feat: 配置 vue-router
- :feat: 支持 rem

## 2019-12-07

- :feat: 支持 eslint，保存时自动 fix
- :feat: 支持 stylelint，保存时自动 fix
- :feat: 完成 webpack 打包配置,分别是 pro，dev
- :feat: 增加 changelog 文件
- :feat: 增加 reset 样式和浏览器判定通用函数
- :feat: 增加 vscode 配置文件信息
- :feat: 增加 gitignore 忽略规则
- :feat: 增加 prettier 配置文件恩建
- :feat: 增加 postcss 配置文件
- :feat: 增加 babel 配置文件
- :feat: 增加 editorconfig 配置文件
- :feat: 增加文件处理 loader
- :feat: 增加 lodash
- :feat: 增加 qs
- :feat: 增加 vant
- :feat: 增加 axios

* :fix 解决 eslint 和 prettier 不兼容问题

## 2019-12-11

- feat: 支持别名,解决 eslint 和别名冲突问题
- 更新 setting 配置文件
