{
"eslint.validate": [
"javascript",
"javascriptreact",
{
"language": "typescript"
},
{
"language": "typescriptreact"
},
{
"language": "vue",
"autoFix": true
},
],
"[javascript]": {
"editor.defaultFormatter": "esbenp.prettier-vscode"
},
"window.zoomLevel": 0,
"[scss]": {
"editor.defaultFormatter": "esbenp.prettier-vscode"
},
"[html]": {
"editor.defaultFormatter": "esbenp.prettier-vscode"
},
"[vue]": {
"editor.defaultFormatter": "octref.vetur"
},
"[jsonc]": {
"editor.defaultFormatter": "esbenp.prettier-vscode"
},
"fileheader.configObj": {
"autoAdd": false
},
"[json]": {
"editor.defaultFormatter": "esbenp.prettier-vscode"
},
"eslint.autoFixOnSave": true,

    "svgviewer.transparencygrid": true,

    "svgviewer.enableautopreview": true,

    "svgviewer.previewcolumn": "Beside",

    "svgviewer.showzoominout": true,
    "editor.formatOnSave": true,
    "px2rem.rootFontSize": 75

}
